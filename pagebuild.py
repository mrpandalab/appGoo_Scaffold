# pagebuild.py
#
# Converts an appGoo file into a PostgreSQL object

import os
import sys
import re

ESC_START = '<%'
ESC_END = '%>'
ECHO = f"{ESC_START}= "
INCLUDE = f"{ESC_START}@ "
SECTION = f"{ESC_START}% "
ESC_STARTS = [f"{ESC_START} ", f"{ESC_START}\n"]
ESC_ENDS = [f" {ESC_END}", f"\n{ESC_END}"]
INCLUDE_PATTERN = (r'<%@\s*(\S*?)\s*%>', r'<%@ \1 %>')
REPLACE_PLACEHOLDER = '__*&&_'
INDENT = ' ' * 4

def build_options(file_options):
    options = {
        'openclose': '$$$_appgoo_$$$',      # must start with $ and end with $
        'comments': False,                  # only t/true/1/y or f/false/0/-1/n
        'whitespace': False,                # ditto
        'array': '__ag_array_',             # must start with __
        'language': 'plpgsql',
        'security': 'definer',
        'return': 'string',                 # str or json
        'drop': 'this',                     # this, all, name, owner.name
        'page_prefix': 'ag_',                # must start with _ or letter and cannot be longer than 7 chars
        '__validation_errors': []
    }

    #If the file contains any option references, then replace in options dict
    strings = file_options.split('\n')
    for string in strings:
        try:
            kv = string.split('=')
            kv = [x.strip().lower() for x in kv]
            if kv[0] == 'openclose':
                if kv[1].startswith('$') and kv[1].endswith('$') and len(kv[1]) > 1:
                    options['openclose'] == kv[1]
                else:
                    raise ValueError(f"{kv[1]} is not a valid value for openclose")
            elif kv[0] == 'comments':
                if kv[1] in ('t', 'true', '1', 'y', 'yes'):   # No need to do False as it is the default
                    options['comments'] = True
                elif kv[1] not in ('f', 'false', '0', '-1', 'n', 'no'):
                    raise ValueError(f"{kv[1]} is not a valid value for comments")
            elif kv[0] == 'whitespace':
                if kv[1] in ('t', 'true', '1', 'y', 'yes'):
                    options['whitespace'] = True
                elif kv[1] not in ('f', 'false', '0', '-1', 'n', 'no'):
                    raise ValueError(f"{kv[1]} is not a valid value for whitespace")
            elif kv[0] == 'array':
                if kv[1].startswith('__'):
                    options['array'] = kv[1]
                else:
                    raise ValueError(f"{kv[1]} is not a valid value for array")
            elif kv[0] == 'return':
                if kv[1] in ('json', 'void'):
                    options['return'] = kv[1]
                elif kv[1] not in ('text', 'txt', 'str', 'string'):
                    raise ValueError(f"{kv[1]} is not a valid value for 'return'")
            elif kv[0] == 'drop':
                options['drop'] = kv[1]
            elif kv[0] == 'page_prefix':
                pattern = r'^[_a-zA-Z]\w{0,6}$'
                if re.match(pattern, kv[1]):
                    options['page_prefix']
                else:
                    raise ValueError(f"{kv[1]} is not a valid value for page_prefix")
            elif kv[0].startswith('#') or len(kv[0]) == 0:
                # Comments & Blank lines are allowed
                pass
            else:
                raise ValueError(f"Option {kv[0]} is not recognised as a valid option")

        except:
            options['__validation_errors'].append(f"options validation failure: {string}")

    return options


def get_section_title(text):
    pattern = SECTION + '(.*?)' + ESC_END
    match = re.search(pattern, text)
    if match:
        return match.group(1).strip()
    else:
        return None


def get_section(source):
    """ extract the section from the source_text and return
        the status, section text, and the remainder
    """

    result = {'status': True, 'section': None, 'before_section': None, 'section_text': '', 'after_section': None}
    section_start = source.find(SECTION)
    if section_start == -1:
        result['before_section'] = source
    else:
        try:
            result['before_section'] = source[:section_start] if section_start > 0 else None
            section_end = source.find(ESC_END, section_start)
            if section_end == -1:
                result['section_text'] = source
                result['status'] = False
            else:
                section_name = source[section_start+len(SECTION):section_end].strip()
                section_end = source.find(ESC_END, section_start+len(SECTION))
                if section_end == -1:
                    result['status'] = False
                else:
                    result['section'] = source[section_start:section_end].strip(SECTION).strip(ESC_END).strip().lower()
                    next_section_start = source.find(SECTION, section_end+len(ESC_END))
                    if next_section_start == -1:
                        result['section_text'] = source[section_start:]
                    else:
                        result['section_text'] = source[section_start:next_section_start]
                        result['after_section'] = source[next_section_start:]
        except:
            result['status'] = False

    return result


def lower_section_heading(match):
    return SECTION + match.group(1).lower() + ESC_END

def build_pg_create_definition(program_type, name, options):
    """ Build a text string for the program declaration and
        return as string
    """
    rtn = ''
    if program_type == 'pfunc':
        rtn = f"CREATE OR REPLACE FUNCTION {name} (_json_in IN JSON)\nRETTYPE {options['return']}\nAS {options['openclose']}\n" \
            + f"DECLARE\n{INDENT}{options['array']} TEXT[];{REPLACE_PLACEHOLDER}DECLARE{REPLACE_PLACEHOLDER}\n" \
            + f"BEGIN\n{REPLACE_PLACEHOLDER}BODY_TEXT{REPLACE_PLACEHOLDER}\nEND;\n{options['openclose']}\nLANGUAGE {options['language']}\n" \
            + f"VOLATILE\nNOT LEAKPROOF\nCALLED ON NULL INPUT\nSECURITY {options['security'].upper()}\nPARALLEL UNSAFE\nCOST 100\nROWS 1;\n"
    elif program_type == 'pproc':
        rtn = f"CREATE OR REPLACE PROCEDURE {name}_p (_json_in IN JSON, _text_out_ OUT {options['return']})\n" \
            + f"LANGUAGE {options['language']}\nSECURITY {options['security'].upper()}\n" \
            + f"VOLATILE\nCALLED ON NULL INPUT\nCOST 100\n" \
            + f"AS {options['openclose']}\n" \
            + f"DECLARE\n{INDENT}{options['array']} TEXT[];{REPLACE_PLACEHOLDER}DECLARE{REPLACE_PLACEHOLDER}\n" \
            + f"BEGIN\n{REPLACE_PLACEHOLDER}BODY_TEXT{REPLACE_PLACEHOLDER}\nEND;\n{options['openclose']};\n" \
            + f"\n" \
            + f"CREATE OR REPLACE FUNCTION {name} (_json_in IN JSON)\nRETTYPE {options['return']}\nAS {options['openclose']}\n" \
            + f"DECLARE\n{INDENT}_proc_out_ {options['return']};\n" \
            + f"BEGIN\n{INDENT}CALL {name}_p(_json_in, _proc_out_);\n" \
            + f"{INDENT}RETURN _proc_out_;\nEND;\n{options['openclose']}\nLANGUAGE {options['language']}\n" \
            + f"VOLATILE\nNOT LEAKPROOF\nCALLED ON NULL INPUT\nSECURITY {options['security'].upper()}\nPARALLEL UNSAFE\nCOST 100\nROWS 1;\n"

    return rtn


def build_pg_object(source, recursive=9):
    """ Convert a source file into Postgres SQL Statement to build the object.
        Can be a procedure or function or any pg object
    """


    # GET FILE
        # GET FILE & LOAD ENTIRE VALUE INTO STRING
    if os.path.exists(source):
        with open(source, 'r') as file:
            source_text = file.read()
    else:
        print(f"{source} is not a valid include file\n")
        sys.exit(-1)

    #source_text = ''

    # DO INCLUDES
    inc_map = {
        #'incl/file/ref': 1,
    }

    include_count = (0,0)
    recursive_count = 0
    source_text = re.sub(INCLUDE_PATTERN[0], INCLUDE_PATTERN[1], source_text)

    for i in range(1, recursive):
        start_indices = []
        start = 0
        while True:
            start = source_text.find(INCLUDE, start)
            if start == -1:  # No more occurrences
                break
            start_indices.append(start)

        if not start_indices:
            # This means that no includes were found
            break

        while True:
            if not start_indices:
                break
            else:
                include_start = start_indices.pop()
                l = len(source_text) + 1
                for e in ESC_ENDS:
                    x = source_text.find(e, include_start)
                    if x != -1:
                        l = min(l, x)

                include_ref = source_text[include_start:l+include_start]
                include_file_path = include_ref.strip(INCLUDE).strip(ESC_END).strip()
                # check that this is a valid file path, if not, error and exit
                if os.path.exists(include_file_path):
                    with open(include_file_path, 'r') as file:
                        file_content = file.read()
                        source_text.replace(include_ref, file_content)
                else:
                    print(f"{include_file_path} is not a valid include file (ref={include_ref})\n")
                    break

        i += 1
        source_text = re.sub(include_pattern[0], include_pattern[1], source_text)

    if source_text.find(INCLUDE) > -1:
        print(f"Too many recursive includes. Build failed.\n")
        sys.exit(-1)


    # Clean text
    patterns = [(r'<%%\s*(\S*?)\s*%>', r'<%% \1 %>'), (r'<%=\s*(\S*?)\s*%>', r'<%= \1 %>'), (r'<%\s*(\S*?)\s*%>',r'<% \1 %>')]
    for pattern in patterns:
        source_text = re.sub(pattern[0], pattern[1], source_text)

    pattern = SECTION + '(.*?)' + ESC_END
    source_text = re.sub(pattern, lower_section_heading, source_text)

    # Split the file into SECTIONS
    # The following splits the source and keeps the delimiter in the list item
    pattern = f"(?={SECTION})"
    sources = re.split(pattern, source_text)[1:]
    if not sources:
        print('invalid file format - no sections found. Build failed.\n')
        sys.exit(-1)
    else:
# OPTIONS -- but is optional
        current_section = get_section_title(sources[0])
        if current_section.startswith('opt'):
            print('getting options from file')
            options = build_options(sources[0][sources[0].find(ESC_END)+len(ESC_END):].strip())
            sources.pop(0) # Remove from sources as it is no longer required
            if sources:
                current_section = None
                current_section = get_section_title(sources[0])
            else:
                print('Missing mandatory program definition section. Build failed')
                sys.exit(-1)
        else:
            print('using default options')
            options = build_options('')

        for e in options['__validation_errors']:
            print(e)

        if len(options['__validation_errors']) > 0:
            print('Build failed due to invalid options specified\n')
            sys.exit(-1)

# PROGRAM DEFINITION - Mandatory
    if current_section[:4] not in('page','help','ppro','pfun','hpro','hfun'):
        print(f"Found section '{current_section}' instead of PAGE_FUNCTION (PFUNC) or PAGE_PROCEDURE (PPROC) or HELPER_FUNCTION (HFUNC) or HELPER_PROCEDURE (HPROC). Build failed.")
        sys.exit(-1)
    else:
        words = current_section.split(' ')
        program = ''
        if len(words) != 2:
            print(f"Invalid format for program definition: '{current_section}'. Build failed")
            sys.exit(-1)
        else:
            # Program Type
            if words[0].find('func') > -1:
                program_type = 'function'
            elif words[0].find('proc') > -1:
                program_type = 'procedure'

            # Program Name
            progs = words[1].split('.')
            if len(progs) == 1:
                program = options['page_prefix'] + progs[0]
            elif len(progs) == 2:
                program = progs[0] + '.' + options['page_prefix'] + progs[1]
            else:
                print(f"Invalid format for program name specified: '{words}'. Build failed")
                sys.exit(-1)

        if program_type == 'function':
            print(build_pg_create_definition('pfunc', program, options))
        elif program_type == 'procedure':
            print(build_pg_create_definition('pproc', program, options))
        else:
            print("....something is not right...")

        print(f"program={program}")


# DECLARE -- but is optional



    print(f"options={options}\n")
    for s in sources:
        print(f"{get_section_title(s)}: {s[s.find(ESC_END)+len(ESC_END):].strip()}\n")


if __name__ == "__main__":
    build_pg_object('newtest1.txt')


