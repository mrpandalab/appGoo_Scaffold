# appGoo Page Code Structure
This is the explanation of how an appGoo Page file is to be structured so that it may be built by the builder in the destination database. Note that anything enclosed in [brackets] is deemed optional. Anything prefixed with a \*star is considered a default value

### OVERVIEW
The files are text files that can be edited in any text editor. The page has a required order for sections and allows multiple alternates of function code or HTML output. The build function will transform the source file into an output that can be loaded into Postgres to create a Stored Procedure in any compatible language. Whilst the most common is pgplsql, it could be Tcl, Javascript or Python or others. An overview the format of the source file is below and then each attribute is explained in further detail below:

```sql
<%% OPTIONS %>
comments=True

<%% PAGE_|HELPER_FUNCTION|PROCEDURE [owner.]name %>
# If it is a procedure, then also build a wrapper function that calls the procedure and returns its return variable.
# The wrapper function always get the above name, the procedure would be the function name + _PROC appended

# For explanation of procedure definition, see https://www.postgresql.org/docs/current/sql-createprocedure.html
# or                                           https://www.postgresql.org/docs/current/sql-createfunction.html

language=[plpgsql]
security=[invoker]|definer
behaviour=[volatile]|immutable|stable
parallel=safe|[unsafe]|restricted
cost=[100]
params=[json_in JSON]
validation={
    named_only: False // raise an error if there are other columns in the json not in the columns below
    columns: [name, ignore, data_type, required, max_length],
             [example, False, text, mandatory, 50],
             [another, True, integer, optional, 0]
} // If supplied, this adds code to validate that all the parameters have been supplied and are valid values

<%% DECLARE %>
    variable data_type := default;

<%% CODE %>
<%@ /path/to/file %>        // this can be anywhere in the file
<%= echo variable value %>  // this can only be in the <%% CODE %> section
<%  code to run %>          // this can only be in the <%% CODE %> section
string to include
<%?/ The '/' will ensure that the <%? or %> will be included as text in the output. The ? refers to anything except space,
the '/' is not included in the output. Therefore, if the user wants '<%@/' in their output they should type '<%@//',
and '//%>' for '/%>' in the output /%>
```

### BLANK LINES
If <%% OPTIONS %>.whitespace=False then all blank lines are removed. Note that lines are trimmed and checked if length=0 to determine a blank line


### COMMENTS
All comments are on a single line only and the first character of the line must be a #. There are no multi-line comments

```sql
# Comments can appear anywhere in the code and are potentially removed from the built version if <%% OPTIONS %%>.comments=True
```

If <%% OPTIONS %>.comments=False, then all comments lines are removed. Note that inline comments are preserved

### ESCAPE FORMATS
All escapes start with ```<%``` and are then followed by a unique character being ```%``` ```@``` ```=``` or no value. If you need to use '<%' in your code, then append with a '/' like ```<%/``` or ```<%@/``` etc... This will resolve to ```<%``` ```<%@``` in your code. If you need '<%@/' in your code, you should write ```<%@//```.

All escapes end with ```%>```. There should be a space or EOL after the opening escape sequence and before the closing escape sequence. For example ```<%=greeting %>``` or ```<% if a = 'hello' THEN%>``` is incorrect as there is no space between the code and the escape sequence. The following is acceptable:

```
<%
    if a = 'hello' THEN
%>
```

as there is an EOL between the escape sequence and the code.

Escape sequences cannot be embedded within an escape sequence with the exception of ```<%@ path/to/file/to/include %>```. Regardless of location, the 'include' directive will insert the contents of the file exactly at the starting point of the escape sequence.

Note that included files can have other includes. However, to avoid recursive inclusion of files that include each other, then appGoo will not perform more than the nominated number of recursive loops to include files. The default is 9 and this can be overridden in the options. The build will fail if the number of loops required exceeds the option value.


### PAGE SECTIONS
These are signified by <%% SECTION_NAME %>. The following sections are available:
* OPTIONS - Optional. This permits you to overwrite the defaults that are applied to the creation of the procedure or function
* PAGE or HELPER FUNCTION or PROCEDURE. Specifies what is being created. Can either be a function or a procedure that is either referenced by other code (HELPER) or returns a result to the web page (PAGE). Potential values are HELPER_FUNCTION, HELPER_PROCEDURE, PAGE_FUNCTION, PAGE_PROCEDURE. It is also mandatory to supply the name of the function|procedure which will be created under the default owner unless otherwise specified. Note that PAGE functions|procedures also have a prefix prepended that can be overwritten in the OPTIONS section. PAGE Procedures also have a corresponding PAGE Function created that returns the result of the PAGE Procedure, you do not need to code that, it is already done for you.
* DECLARE - Optional. Specify any parameters that you require to be used in your code. These do not have to be indented but do need to be grammatically correct as this is not validated.
* CODE. This is where the code and escaped strings are to be entered.

Only the first 3 letters of the option are verified and is case insensitive, therefore ```<%% OPT %>``` == ```<%% options %>```, and ```<%% DECLARE %>``` == ```<% Declarations of the bestest kind %>``` and ```<%% cod %>``` == ```<%% CODING %>``` == ```<%% Coding begins here %>``` and so forth. However, the same is not true for ```<%% PAGE_FUNCTION ... %>, <%% Page_Procedure ... %>, <%% Helper_Function %> and <%% HELPER_PROCEDURE %>``` where case-insensitive abbreviations of ```PPROC PFUNC HPROC HFUNC``` are allowed, otherwise full case-insensitive values are required.


### OPTIONS - Optional
The options allow override of the defaults applied by appGoo. Note that all values are validated and may cause runtime errors. Below are the options, the value that is applied by appGoo if no option is specified is shown in [brackets]. For example, if the 'comments' option is not specified, then the value of ```False``` will be applied.

```sql
openclose=[$$$_appgoo_$$$]  // the dollar quoting used to encapsulate the code
comments=[false]            // whether to include (true) or remove comments from compiled code. Only happens in <%% CODE %> section
whitespace=[false]          // whether to include (true) or remove blank lines from compiled code. Only happens in <%% CODE %> section
recursive=[9]               // maximum number of recursive loops to check for 'include file' references
array=[__ag_array_]         // the name of the array to add output to
language=[plpgsql]          // the language of the object, could be plpgsql, sql, python or any supported postgresql language installed
return=[text]               // the return data_type to send back to calling entity. Text, string, json, void
security=[definer]          // either 'definer' or 'invoker' to determine whom to run the function as
drop=[this]|all|name|owner.name  //this = drop with this parameter only, all= all parameters for this owner, name or owner.name comma delimited
page_prefix=ag_             // this is prefixed to the name of the page function|procedure
```

The format of options is option=value where there is no space allowed. All text is converted to lowercase.


### VARIABLE DECLARATION (optional)
You may specify additional variables here if required by your code. Your build will fail if you specify the variable name used by appGoo for an array used in building the code. All variables must start with either a single underscore '\_' or a letter. If a variable starts with more than 1 underscore or anything that is not a-Z, then build will fail. Note that all indentation entered is removed. If your line does not complete with a semi-colon ';', it will be appended, otherwise the grammer of the declaration is not checked.

An example is:

```sql
<%% DECLARE %>
    i integer := 0
    t text;
    t2 text
    _x__ text

```


### START OF CODE

```
<%% CODE %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>appGoo Test</title>
</head>
<body>
    <p>Hello <%= initcap(name) %>, how are you ?</p>
```

After you start the CODE section, the immediate output is boilerplate HTML, you must then escape to code using the ```<% insert code here %>``` or echo a variable's value with ```<%= variable_name %>```.

Note that ```<%@ include/file/here %>``` can be inserted anywhere including within code escapes.


### CODE


```sql
<%
   if i = 0 then
     i := 1;
   end if;
%>
```

* This is code that can be executed by the database. The '<%' must be followed by a space or a new line. The '%>' must be preceded by a space or a new line.
* Any code that is legal for the destination database can follow. No code validation is performed by the build. Only blank lines and comments are removed (as determined by settings in OPTIONS).
* Note that if you deliberately want blank lines as part of a text string, use a character representation rather than actual blank lines in the code. Within PostgreSQL this would be represented as chr(13). For example, myText = concatenate('I am now going to show a blank line ', chr(13), chr(13), 'and goodbye.')
* Your indentation will not be altered in any way and lines can commence with spaces or a tab character
* The code will be built exactly as you enter it without any alteration. You must perform your own indentation, appGoo will not lint it for you.


### TEMPLATE TEXT
Any text entered outside of the code escapes will be considered a templated string that is returned. Note that the code escapes can be used inline providing that they have a space after/before the % (<% %>). Example code with template text:


```sql
<ul class="actions">
    <li class="actions__item dropdown">

<% if v_user_id = -1 then %>                
        <div id="div_cog_administrator" class="dropdown-menu dropdown-menu--right" style="display:none;">
<% else %>
        <div id="div_cog_user" class="dropdown-menu dropdown-menu--right" style="display:none;">
<% end if; %>
            <h4>Actions</h4>
                <ul class="actions-list" role="menu">
                           ...
```

### ECHO/PRINT A VALUE
A special case is catered for to allow the value of a parameter or variable or recordset field to be inserted as a value in the text. This is done by starting with an inline <%= like in the following example:

```sql
<ul class="actions">
    <li class="actions__item dropdown">
        <div id="div_cog_<%= v_user_type %>" class="dropdown-menu dropdown-menu--right" style="display:none;">
```

In the above example, if the user requesting the page had a user type of 'administrator', then the text returned would be:

```html
        <div id="div_cog_administrator" class="dropdown-menu dropdown-menu--right" style="display:none;">
```


Otherwise if their user type was user the result would be 

```html
        <div id="div_cog_user" class="dropdown-menu dropdown-menu--right" style="display:none;">
```


### END OF SECTIONS
There is no end of section marker, the start of a new section signifies the end of the prior section. The end of the file is the end of code. If you have a '<%' or '<%=' open then this will be closed for you with a new line and '%>'


### DROPPING FUNCTIONS
By default, the build program will replace the function that has the same parameter types and count as the function to build in the specified schema. This ensures that overloaded functions are not dropped. If you change the parameter types or count, the built function will remain in the database. To help you control this situation, the build program has an escape directive that can automate this. The following values can be referenced between the parameters of your page and the declaration of variables:

* <%drop %all %> -- this drops all functions of this name of all schemas regardless of parameter definition
* <%drop %none %> -- do not drop any function before creating this function. This is the default behaviour if no directive is specified, the build program will issue a 'CREATE AND REPLACE FUNCTION' statement
* <%drop parameter_type, parameter_type ... parameter_type %> -- only drop functions of the current function name that have this parameter definition. This applies across all schemas

Note that the 'drop by parameter type' option can be specified multiple times. If no function can be found that matches the parameter types then an error will not be encountered. There is also another directive for stating prior names of the functions. When specified, any functions of that name (with an optional qualified schema) will be dropped regardless of parameters. Be careful as this will drop functions across all schemas that you have permission to drop functions within. Its syntax is:

* <%dropname name1, name2, [schema].name3 ... name %>

The 'dropname' directive can be repeated many times.

Some examples:

```sql
<%drop text, varchar, boolean, text, integer %>
<%dropname old_function, my_data.very_old_function %>
```


### ESCAPING ESCAPE CHARACTERS
If you have a genuine need to include either '<%' or '%>' in your code, then identify this with a '/'. For opening escapes, append the '/', for closing, prefix the '/'. For example:

```sql
v_example = '<%/ hello there //%>';
another_example = '<%/include nothing /%>';
```

This will result in the following being included in your code:

```sql
v_example = '<% hello there /%>';
another_example = '<%include nothing %>';
```

If you need to include the actual '<%/' text in your code, then this would also require an additional '/' as shown:

```sql
v_example = '<%// hello there //%>';
```

which would produce:

```sql
v_example = '<%/ hello there /%>';
```

### INCLUDING OTHER CODE (optional)
If you have code that is common across multiple files then you can maintain the source file and include it in others. Note that this capability may cause inconsistencies if you just build by updated files only. Note that the file is inserted as-is, therefore will inherit the current code escaping in place. This results in the ability to include template text as well as code. This capability is ideal if you have common error handling or options that you wish to specify

```sql
<%@ /path/from/project/basepath/to/file.snippett %>
```

* The file can be of any extension, we recommend that it is unique and easily identifies that it is included in other files
* Note that once the file is inserted it is resolved like any other text in the source file. This results in the ability to include other files from within the included file. Therefore a recursive potential exists, refer to explanation in OPTIONS regarding recursive allowances
* If the file cannot be found then build for this file will fail
* Only text files can be specified, empty files are allowed, but non-text files will cause a build failure for this file.
* includes can be performed ANYWHERE in the file and are fully resolved before the page is built for the database. You can use includes for common parameters, common variables, common code, common html, common javascript scripts, error behaviour, options, the choice is yours.


### WHEN TO USE FUNCTIONS v PROCEDURES
You may wish to consider using a PROCEDURE when you wish to perform COMMIT/ROLLBACK during the code rather than at the conclusion of the code (which is the functionality of a FUNCTION). Without that requirement, we would recommend to use a FUNCTION.
